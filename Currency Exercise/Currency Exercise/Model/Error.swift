import Foundation

struct AuthErrorResponse: Codable {
    let error: String?
}

struct ErrorEntity: Codable {
    let error: ErrorResponse?
}

struct ErrorResponse: Codable {
    let error_data: Int?
    let error_msg: String?
}

