//
//  SymbolResult.swift
//  Currency Exercise
//
//  Created by Shivang Pandya on 02/12/22.
//

import Foundation

struct SymbolResult: Codable {
    let success : Bool?
    let symbols : [String:String]
}

struct CurrentcyModel {
    let currencyCode: String?
    let currencyName: String?
}
