//
//  ConvertResult.swift
//  Currency Exercise
//
//  Created by Shivang Pandya on 05/12/22.
//

import Foundation

struct ConvertResult: Codable {
    let success : Bool?
    let date : String?
    let result : Double?
}
