//
//  ViewController.swift
//  Currency Exercise
//
//  Created by Shivang Pandya on 29/11/22.
//

import UIKit

class ViewController: UIViewController {

    var currencyArray = [CurrentcyModel]()
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    
    @IBOutlet weak var rightSideLbl: UILabel!
    @IBOutlet weak var leftSideLbl: UILabel!
    
    @IBOutlet weak var leftTextField: UITextField!
    @IBOutlet weak var rightTextField: UITextField!

    var currentLeftIndex = 0
    var currentRightIndex = 0
    
    var curLeftSelection = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        leftTextField.delegate = self
        rightTextField.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        getAllCurrenciesAPI()
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.view.endEditing(true)
    }
    
    // MARK: - Button click Events
    @IBAction func LeftButtonClick(_ sender: Any) {
        curLeftSelection = true
        
        picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.isTranslucent = true
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
    }
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        if currencyArray.count > 0 {
            if(curLeftSelection){
                leftSideLbl.text = self.currencyArray[currentLeftIndex].currencyCode
            }else{
                rightSideLbl.text = self.currencyArray[currentRightIndex].currencyCode
            }
            leftTextField.text = ""
            rightTextField.text = ""
        }
    }
    @IBAction func RightButtonClicked(_ sender: Any) {
        curLeftSelection = false
        
        picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
                
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.isTranslucent = true
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
    }
    
    @IBAction func swapButtonClick(_ sender: Any) {
        leftSideLbl.text = self.currencyArray[currentRightIndex].currencyCode
        rightSideLbl.text = self.currencyArray[currentLeftIndex].currencyCode
        
        let leftTxt =  leftTextField.text
        let rightTxt =  rightTextField.text
        
        leftTextField.text = rightTxt
        rightTextField.text = leftTxt
    }
    
    @IBAction func DetailButtonClick(_ sender: Any) {
        
    }
    
}

// MARK: - Textfield Delegate

extension ViewController : UITextFieldDelegate  {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        if textField == leftTextField {
            rightTextField.text = ""
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print(leftTextField.text)
        //print(rightTextField.text)
        if (leftTextField.text?.count ?? 0 > 0){
            self.getconvertedValueAPI(to: self.rightSideLbl.text ?? "To", from: self.leftSideLbl.text ?? "From", amount: (leftTextField.text ?? "1"))
        }
        
    }
}
// MARK: - PickerView Delegate

extension ViewController : UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
        
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.currencyArray.count
    }
        
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.currencyArray[row].currencyCode
    }
        
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //print(self.currencyArray[row].currencyCode)
        if(curLeftSelection){
            currentLeftIndex = row
        }else{
            currentRightIndex = row
        }
    }
}

// MARK: - API Calls
extension ViewController {
    // MARK: - for get all currencies
    func getAllCurrenciesAPI(){
        MyService().getCountries { result in
            switch result {
            case .success(let data):
                let symbolResult = data
                
                for currency in symbolResult?.symbols ?? [:]  {
                    let myObject  = CurrentcyModel(currencyCode: currency.key , currencyName: currency.value )
                    self.currencyArray.append(myObject)
                }
                print(self.currencyArray.count)
            case .failure:
                print("error")
            }
        }
    }
    
    // MARK: - for get all currencies
    func getconvertedValueAPI(to:String,from:String,amount:String){
        
        MyService().getConversion(from: from, to: to, amount: amount) { result in
            switch result {
            case .success(let data):
                let convertResult = data
                self.rightTextField.text = "\(convertResult?.result ?? 0.0)"
                print(self.rightTextField.text)
            case .failure:
                print("error")
            }
        }
        
    }
}
