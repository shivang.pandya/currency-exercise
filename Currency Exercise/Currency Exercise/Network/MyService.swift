import Foundation
import UIKit

typealias MySymbolResult = (Result<SymbolResult?, NSError>) -> Void
typealias MyConversionResult = (Result<ConvertResult?, NSError>) -> Void

final class MyService {
    let apiCall: APICall
    
    init(apiCall: APICall = APICall()) {
        self.apiCall = apiCall
    }
    
    func getCountries( completionHandler: @escaping MySymbolResult) {
        let api = API.getCountrySymbols
        APICall().sendRequest(api) { result in
            switch result {
            case .success(let data):
                if let reponse = try? JSONDecoder().decode(SymbolResult.self, from: data) {
                    completionHandler(Result.success(reponse))
                }
            case .failure(let error):
                completionHandler(Result.failure(error as NSError))
            }
        }
    }
    
    func getConversion(from:String , to:String , amount:String, completionHandler: @escaping MyConversionResult) {
        let api = API.getConver(from: from, to: to, amount: amount)
        APICall().sendRequest(api) { result in
            switch result {
            case .success(let data):
                if let reponse = try? JSONDecoder().decode(ConvertResult.self, from: data) {
                    completionHandler(Result.success(reponse))
                }
            case .failure(let error):
                completionHandler(Result.failure(error as NSError))
            }
        }
    }
    
}
