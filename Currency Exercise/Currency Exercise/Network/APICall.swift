//
//  APICall.swift
//  Currency Exercise
//
//  Created by Shivang Pandya on 30/11/22.
//

import Foundation
import UIKit

enum HTTPMethod {
	case get, post, put, patch, delete
	
	var description: String {
		switch self {
		case .get:
			return "GET"
		case .post:
			return "POST"
		case .put:
			return "PUT"
		case .patch:
			return "PATCH"
		case .delete:
			return "DELETE"
		}
	}
}
extension String {
    var urlEncoded: String? {
        return addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    }
}
class APICall {
     func getBaseURL(_ api: API) -> String {
         let baseURL = baseurlLive
         
         return baseURL
    }
    
    func sendRequest(_ api: API, handler: @escaping (Result<Data, Error>) -> Void) {
        let baseURL = getBaseURL(api)
        
        guard let url = URL(string: (baseURL + api.method).urlEncoded ?? "") else {
            return
        }
        
        let params = api.params

        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)

        if !params.isEmpty {
            let queryItems: [URLQueryItem] = params.map { parameter in
                    URLQueryItem(name: parameter.key, value: (parameter.value as? String) ?? "")
            }
            
            components?.queryItems = queryItems
        }
        
        var urlRequest = URLRequest(url: (components?.url)!)
        urlRequest.httpMethod = api.httpMethod.description
        urlRequest.addValue(apiKey, forHTTPHeaderField: "apikey")

        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
 //       let params = api.params
        
//        if !params.isEmpty,
//           let data = try? JSONSerialization.data(withJSONObject: params, options: []) {
//            urlRequest.httpBody = data
//        }
//        debugPrint("params for \(url.absoluteString): \(params.description.prefix(1000))")
        
        let task = URLSession.shared.dataTask(with: urlRequest) { data, _, error in
            if let error = error {
                DispatchQueue.main.async {
                    handler(.failure(error))
                }
                return
            }
            if let data = data {
                if let outputStr = String(data: data, encoding: .utf8) as String? {
                    debugPrint("response for \(url.absoluteString): " + outputStr.prefix(30000))
                }
                
                DispatchQueue.main.async {
                    if let reponse = try? JSONDecoder().decode(AuthErrorResponse.self, from: data) {
                        if reponse.error == invalid_token || reponse.error == access_denied || reponse.error == unauthorized {
                            
                        }
                    }
                    handler(.success(data))
                }
            }
        }
        task.resume()
    }

    private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")

        return string
            .addingPercentEncoding(withAllowedCharacters: characterSet)!
            .replacingOccurrences(of: " ", with: "+")
            .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
}
