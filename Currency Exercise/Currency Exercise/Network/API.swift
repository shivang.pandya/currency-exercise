//  API.swift
//  Currency Exercise
//
//  Created by Shivang Pandya on 30/11/22.
//

import Foundation
import UIKit

enum API {
    case getCountrySymbols
    case getConver(from: String,to: String,amount: String)
	

	var httpMethod: HTTPMethod {
        switch self {
        case .getCountrySymbols,
                .getConver:
            return .get
            
        }
	}
    
    var method: String {
        switch self {
        case .getCountrySymbols:
            return "symbols"
        case .getConver:
            return "convert"
            
        }
        
    }

	var params: [String: Any] {
        switch self {
        case let .getConver(from, to, amount):
            return [
                "from": from,
                "to": to,
                "amount":amount
            ]
            
        default:
            return [:]
        }
	}
}
