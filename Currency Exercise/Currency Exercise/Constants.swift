
//  Constants.swift
//  Currency Exercise
//
//  Created by Shivang Pandya on 30/11/22.
//

import Foundation
import UIKit

let SYSTEM_VERSION = UIDevice.current.systemVersion
let SCREEN_WIDTH = UIScreen.main.bounds.width
let SCREEN_HEIGHT = UIScreen.main.bounds.height
let MAIN_WINDOW = UIApplication.shared.windows.first


// MARK: AppName
let appName = "Currency Exercise"


// MARK: BASEURL
var baseurlLive: String {
    return "https://api.apilayer.com/fixer/"
}

let invalid_token = "invalid_token"
let access_denied = "access_denied"
let unauthorized = "unauthorized"

let apiKey = "saqZpbOnHFJlxfWRuHoPj1nAcJOpdaFa"

func IS_IPAD() -> Bool {
    switch UIDevice.current.userInterfaceIdiom {
    case .phone: // It's an iPhone
        return false
    case .pad: // It's an iPad
        return true
    case .unspecified: // undefined
        return false
    default:
        return false
    }
}
