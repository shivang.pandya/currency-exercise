//
//  CustomViewAndButton.swift
//  Currency Exercise
//
//  Created by Shivang Pandya on 03/12/22.
//

import Foundation
import UIKit


class CustomView : UIView {
    
    //initWithFrame to init view from code
      override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
      }
      
      //initWithCode to init view from xib or storyboard
      required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
      }
      
      //common func to init our view
      private func setupView() {
          self.layer.borderColor = UIColor.black.cgColor
          self.layer.borderWidth = 1.0
          self.layer.masksToBounds = true
      }
}
    
class CustomButton : UIButton {
    
    //initWithFrame to init view from code
      override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
      }
      
      //initWithCode to init view from xib or storyboard
      required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
      }
      
      //common func to init our view
      private func setupView() {
          self.layer.borderColor = UIColor.black.cgColor
          self.layer.borderWidth = 1.0
          self.layer.masksToBounds = true
      }
}
